# Description

Installs an arbitrary list of packages.

# License

Released under the Apache License, Version 2.0.
